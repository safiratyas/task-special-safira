function baseSix(number) {
    if(number < 0) {
        console.log('Number must be greater than 0');
    }

    let result = [];
    let temp;

    while(number !== 0) {
        temp = number % 6;
        number = (number - temp) / 6;
        result = [temp, ...result]; 
    }
    return result.join('');
}
console.log(baseSix(2022));