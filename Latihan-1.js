/*  
    SELECT SalesPerson.SalesPersonID, SalesPerson.SalesYTD 
    FROM SalesPerson 
   	JOIN SalesOrderHeader ON SalesPerson.SalesPersonID = SalesOrderHeader.SalesPersonID
    JOIN SalesOrderDetail ON SalesOrderHeader.SalesOrderID = SalesOrderDetail.SalesOrderID
    JOIN Product ON SalesOrderDetail.ProductID = Product.ProductID
    WHERE Product.Name LIKE ‘%Wheel%’
    ORDER BY SalesPerson.SalesYTD DESC
    LIMIT 5
 */