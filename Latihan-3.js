function digitSum(int) {
    const integers = int.toString();
    let sum = 0;
    for(let i = 0; i < integers.length; i++) {
        sum += parseInt(integers[i]);
    }
    return sum;
}

function search(string, part) {
    let result = false;
    for(let i = 0; i < string.length; i++) {
        if(string[i] == part) {
            result = true;
            break;
        }
    }
    return result;
}

function Bull(int) {
    const integers = int.toString();
    let result = false;
    if(int % 4 == 0 || search(integers, '4') || digitSum(int) % 4 == 0) {
        result = true;
    }
    return result;
}

function Dog(int) {
    const integers = int.toString();
    let result = false;
    if(int % 8 == 0 || search(integers, '8') || digitSum(int) % 8 == 0) {
        result = true;
    }
    return result;
}

for(let i = 0; i <= 100; i++) {
    if(Bull(i) && Dog(i)) {
        console.log(`${i} is Bulldog`)
    } else if(Bull(i)) {
        console.log(`${i} is Bull`)
    } else if(Dog(i)) {
        console.log(`${i} is Dog`)
    } else {
        console.log(`${i}`)
    }
}